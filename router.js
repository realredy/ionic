import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
 import {createStackNavigator} from '@react-navigation/stack';
 import Home from './pages/home';
 import Face from './pages/face';
   import Test from './pages/Test';
  
 const Rutas = createStackNavigator();
 const Routerk = ()  => {  
 
  return ( 
             <>
             <NavigationContainer>
             <Rutas.Navigator>
             
             <Rutas.Screen options={{headerShown:false}} name="face" component={Face} />  
             <Rutas.Screen name="Home" component={Home} />
             <Rutas.Screen name="Test" component={Test} />
             </Rutas.Navigator>
             </NavigationContainer>
             </>
  );
}

 

export default Routerk;
