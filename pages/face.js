import React, {useState, useEffect} from 'react';
import { StyleSheet, 
    Text, 
    View ,
    ScrollView, 
    ImageBackground, 
    Image, 
    TextInput, 
    TouchableOpacity
} from 'react-native';

  import {fire} from '../database';
 
import * as Permissions from 'expo-permissions';
import * as Notifications from 'expo-notifications';
// import { NavigationContainer } from '@react-navigation/native';

 of();
  async function of(){
     var permision = await Permissions.askAsync(Permissions.NOTIFICATIONS);
     if(permision.status !== 'granted'){
            return;
          } else { 
      var token = await Notifications.getExpoPushTokenAsync();
        // console.log('token:: ',token.data);
  
        const db = fire.database().ref('expo_ids');
          db.set({  nombre: token.data  }); 
      
      //  fire.database().ref('expo_ids').once('value').then((snapshot) => {
      //   let idsgeteer = (snapshot.val());
      //   let resultado = Object.keys(idsgeteer);
      //         console.log('valor: ',resultado);
      //     for (let meta = 0; meta < resultado.length; meta++) { 
      //       const element = resultado[meta];
      //       let elems = idsgeteer[element];
      //        if(elems.nombre = token){
      //            console.log('existe');
      //        }  else  {
      //         saveId(token);
      //        }
      //     }
      // });
 

          }//wlse
   } 
     
       
           
       
  
  
   
    
 const Face = ({ navigation })  => {   
  let Fecha__Hora = new Date();
 let Data = Fecha__Hora.getHours()+':'+ Fecha__Hora.getMinutes()+' | '+ Fecha__Hora.getDate()+'-'+ Fecha__Hora.getMonth()
     const [datos, setDatos] = useState({
         title: 'Nan',
         body:'Nan',
         data:{id: 10000, mensaje: 'Nan'}
     });
    const [inputtext,setInputtext] = useState();
    // getPermissions();
   
    // function getPermissions(){   
    //     Notifications.addNotificationResponseReceivedListener(response => {
    //         console.log(response.notification.request.content); 
    //  });
    // } 
    const initChat = async () => {
      let idChat = datos.data.id;
     try{ const db = fire.database().ref(idChat); 
       let sendMessage = await db.push({ 
          time: Data,
         nombre: 'Ricardo',
         mensaje: inputtext
        });
        navigation.navigate('Home',{ idChat });
        // console.log(/*Object.keys(sendMessage)*/  sendMessage.val()/*JSON.stringify(sendMessage)*/);
      }catch(er){
        alert(er);
      }
    } 

    useEffect(() => {
        // registerForPushNotificationsAsync().then(token => setExpoPushToken(token));
    
        // This listener is fired whenever a notification is received while the app is foregrounded
        // notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
        //   setNotification(notification);
        // });
      let notificationListener =  Notifications.addNotificationResponseReceivedListener(response => {
        setDatos(response.notification.request.content); 
     });
    
        // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
        let responseListener = Notifications.addNotificationResponseReceivedListener(response => {
          console.log(response);
        });
    
        return () => {
          Notifications.removeNotificationSubscription(notificationListener);
          Notifications.removeNotificationSubscription(responseListener);
        };
      }, []);
     
    return ( 
       <ImageBackground source={ require('../assets/homeBg.jpg') } style={{flex:1}}>
        <ScrollView style={{flex:1}}>
        
        <View style={estilos.bigBox} >
              <View style={estilos.vista_general}>
                  <View style={estilos.BoxMesajeTop}>
                    <Text style={{fontSize:23}}>{datos.title}</Text> 
                    <Text style={{fontSize:13}}>{datos.body}</Text>
                    <Text style={{fontSize:15}}>El ID del Chat es:  {datos.data.id} </Text>
                    <Text style={{fontSize:13}}>Mensaje es: {datos.data.mensaje}</Text>
                    <Text style={{fontSize:13}}>Escribe tu respuesta para iniciar el chat!</Text>
                  </View>
                  
                  <View style={estilos.BoxMesajeBottom}>
                      <TextInput style={estilos.impText} value={inputtext} multiline={true} onChangeText={ _text => setInputtext(_text)}  /> 
                      <TouchableOpacity onPress={initChat}>
                      <Text style={estilos.bottomInicial}>iniciar</Text>
                      </TouchableOpacity> 
                  </View> 
              </View> 
         </View>

        </ScrollView>
        </ImageBackground>
);
}
const estilos = StyleSheet.create({
    bigBox:{  flex:1,
        // resizeMode: "cover",
          justifyContent:'center',
          alignItems:'center', 
         paddingTop: '75%'
        },
     vista_general:{  
        width: '90%',
        height: 350,
        alignItems:'center',   
        backgroundColor:'#CACFD2',
        borderRadius:10,
        paddingBottom:90
     },
     BoxMesajeTop:{
        flex:2,  
        alignItems:'center',
        justifyContent: 'flex-end', 
     },
     BoxMesajeBottom:{
        flex:2,  
        alignItems:'center',
        width:'90%',
        
     },
     impText:{
         width:'100%',
         backgroundColor: '#fff',
         height:45,
         marginTop:15,
         borderRadius:5,
         borderStyle:'solid',
         borderColor: '#979A9A',
         borderWidth:1,
         paddingHorizontal:10
     },
     bottomInicial:{
         width:150,
         height: 40,
         backgroundColor: '#943126',
         borderRadius:4,
         color: '#fff',
         fontSize:18,
         marginTop:15,
         textAlign: 'center',
         lineHeight: 40
     }
     
});


export default Face;
