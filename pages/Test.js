import React, {useState, useEffect, useRef} from 'react';
import { StyleSheet,FlatList, Text, View ,ScrollView, ImageBackground, Image, TextInput, TouchableOpacity} from 'react-native';
import {fire} from '../database';

 
const Test = ({ route })  => {  
    const { idChat } = route.params;
      const [refres, setRefres] = useState(()=>{

        let statet = [];
const db = fire.database().ref(idChat);
db.on('value', async snap  =>  {  
  if(statet != ""){
    statet = [];
  }
      let valor =  snap.val();  
      var objeto = Object.keys(valor);  
      // console.log('objeto: ',objeto);
      for ( var i = 0; i < objeto.length; i++ ) { 
      var todo = objeto[i]; 
      var sale = valor[todo]; 
      statet.push({ 
          time: sale.time,
          mensaje: sale.mensaje,
          nombre: sale.nombre,
          id: todo  
      });  
    }  //for  
     console.log('inittialState: ',statet);  
    setRefres(statet)
}); 
 
//  const state =  Object.create(statet);

 })
   
/*--------------------------*/
 

   
    const realRef = useRef();

  
 
return (
    <View style={{flex:1}}>  
            <View ref={realRef} style={styles.container}> 
             {/* <Text onPress={refe2}>{ 'state' }</Text>   */}
            <FlatList data={refres} 
               renderItem={({item, index}) =>
                <View key={index} style={ item.nombre === 'Ricardo' ? styles.boxChat: styles.boxChat2 }> 
                    {item.nombre == 'Ricardo' ? <Image style={styles.laImage} source={require('../assets/foto_cv.jpg')} /> : <Image style={styles.laImage} source={require('../assets/black_friday.jpg')} />}
                    <View style={ item.nombre === 'Ricardo' ? styles.textChat: styles.textChat2 } > 
                    <Text style={styles.textdate}>{item.nombre} • {item.time} </Text>
                    <Text style={styles.text}>{item.mensaje} </Text>  
                    </View>  
                 </View>  
                }
               />
             
          </View>
    </View>
   )
}


const styles = StyleSheet.create({
   
    container: {
      flex: 1,  
      paddingBottom:50, 
    },
    textdate: { 
      fontSize: 9, 
      paddingTop:10,
      paddingLeft:10,
      paddingRight:10,
    },
    text: { 
      fontSize: 17, 
      paddingBottom:10,
      paddingLeft:10,
      paddingRight:10,
    },
    boxChat:{
       flexDirection:'row-reverse', 
      marginBottom: 10, 
      padding:10, 
      
    },
    boxChat2:{
      flexDirection:'row', 
      marginBottom: 10, 
      padding:10,   
      
    },
    laImage:{
       width:'10%',
       margin: '2.5%',
       height:30,
       minHeight:40,
      //  borderStyle: 'solid',
      //  borderWidth:2,
      //  borderColor: '#fff',
       borderRadius:30,
    },
    textChat:{
      backgroundColor: '#f2fbcce8', 
      width: '85%',
      borderRadius:10,
    },
    
    textChat2:{
      backgroundColor: '#fdf2b2e8', 
      width: '85%',
      borderRadius:10,
    },
    input:{
     backgroundColor: '#fff',
     width:'82%',
     fontSize:20,
     marginLeft:'3%',
     paddingLeft:10,
     height:50,
     marginTop:10,
     borderRadius:25,
     marginBottom:8,
     borderStyle: 'solid',
       borderWidth:1,
       borderColor: '#d5d9de',
       opacity:0.9,
    },
  });
  
export default Test;