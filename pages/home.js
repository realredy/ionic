import React, {useState, useEffect, useRef} from 'react';
import { StyleSheet,FlatList, Text, View ,ScrollView, ImageBackground, Image, TextInput, TouchableOpacity} from 'react-native';
import {Audio} from 'expo-av';
import {fire} from '../database';
 
// import * as Permissions from 'expo-permissions';
// import * as Notifications from 'expo-notifications';



  // async function of(){
  //    var permision = await Permissions.askAsync(Permissions.NOTIFICATIONS);
  //    if(permision.status !== 'granted'){
  //           return;
  //         } else { 
  //     var token = await Notifications.getExpoPushTokenAsync();
  //      console.log('token:: ',token.data);
  //     const db = fire.database().ref('expo_ids');
       
  //     await db.push({ 
  //      nombre: token.data 
  //     }); 

  //         }//wlse
  //  }
    
      
  //   of();
   
       
 const App = ({ route })  => {  
  const { idChat } = route.params;
  //Obtenemos los valores ref del scrollview para que cuando detecte 
  //que ancho cambia este se manntenga en el fondo
  const scrollRef = useRef(); 
  const scrollReaction =()=>{
    // reaccion que mantiene el scroll en el fondo
    scrollRef.current.scrollToEnd();
  }
  const [message , setMessage] = useState(''); 
  const [refres, setRefres] = useState(()=>{
  
    let statet = [];
  const db = fire.database().ref(idChat);
  db.on('value', async snap  =>  {  
  if(statet != ""){
  statet = [];
  }
  let valor = await snap.val();  
  var objeto = Object.keys(valor);  
  var objeto2 = Object.keys(valor).length; 
  playSound();
  for ( var i = 0; i < objeto.length; i++ ) { 
  var todo = objeto[i]; 
  var sale = valor[todo]; 
  statet.push({ 
      time: sale.time,
      mensaje: sale.mensaje,
      nombre: sale.nombre,
      id: todo  
  });  

  }  
  setRefres(statet)
  }); 
  
  //  const state =  Object.create(statet);
  
  });
  
  /*--------------------------*/
   
//    function getPermissions(){   
//      Notifications.addNotificationResponseReceivedListener(response => {
//          console.log(response.notification.request.content.title);
//         // setresetData(response.notification.request.content.title);
//     const db = fire.database().ref('4106');
//    let record = db.push({ 
//         nombre:response
//     }); 
//   });
// }
  
  // // Notifications.addNotificationsDroppedListener(listenerf  => {
  // //   // console.log('notificacion saliente::::',listenerf);
  // // }) 
   
  // // // This listener is fired whenever a notification is received while the app is foregrounded
  // //  Notifications.addNotificationReceivedListener(notification => {
  // //   // setNotification(notification);
  // //   // setresetData(notification);
  // //   // alert(notification);
  // //   // console.log('notificacion saliente::::',notification);
  // // });
  //   } 
   
  /*
   *@play sound in messages
   *
   */
//  async function playSound() {
//     console.log('Loading Sound');
//     const  Misound = new Audio.Sound();
       
//     Misound.loadAsync(require('../assets/appointed-529.mp3'))
      
//     console.log('Playing Sound');
//     await Misound.playAsync(); }

    async function playSound() {
      const { sound } = await Audio.Sound.createAsync(
         require('../assets/served-504.mp3')
      );
      await sound.playAsync(); }
     

const  sendMessage = async () => { 
  try{ 
      var fin = new Date();
      const db =  fire.database().ref(idChat);
      let record = await db.push({
      time: fin.getHours()+':'+ fin.getMinutes()+' | '+ fin.getDate()+'-'+fin.getMonth(),
      nombre:'Ricardo',
      mensaje: message
      });  
      setMessage('');
      // refe2();  
        } catch (error){
        console.log(error);
        }
 }



 
 
  
  return ( 
            
  
         <ImageBackground source={ require('../assets/chat_bg.jpg') } style={{flex:1, resizeMode: "cover",
         justifyContent: "center"}} >
         
         <ScrollView ref={scrollRef} onContentSizeChange={scrollReaction}>
            <View style={styles.container}> 
            <FlatList data={refres}
               
               renderItem={({item}) =>
                <View  style={ item.nombre === 'Ricardo' ? styles.boxChat: styles.boxChat2 }> 
                    {item.nombre == 'Ricardo' ? <Image style={styles.laImage} source={require('../assets/foto_cv.jpg')} /> : <Image style={styles.laImage} source={require('../assets/black_friday.jpg')} />}
                    <View style={ item.nombre === 'Ricardo' ? styles.textChat: styles.textChat2 } > 
                    <Text style={styles.textdate}>{item.nombre} • {item.time} </Text>
                    <Text style={styles.text}>{item.mensaje} </Text>  
                    </View>  
                 </View>  
                
                }
               />
          </View>
          </ScrollView> 
              <View style={{flexDirection:'row', height:60, alignItems:'center', justifyContent:'center' }}>
                 <TextInput style={styles.input}  value={message} multiline={true} onChangeText={text => setMessage(text)} />
                 <TouchableOpacity onPress={sendMessage} style={{width:'15%'}}>
                   <Image style={{width:'90%', height:40}} source={require('../assets/send.png')} /></TouchableOpacity>      
                  
                </View> 
              </ImageBackground>
          
        
  );
}


const styles = StyleSheet.create({
   
  container: {
    flex: 1,  
    // paddingBottom:50, 
     
  },
  textdate: { 
    fontSize: 9, 
    paddingTop:10,
    paddingLeft:10,
    paddingRight:10,
  },
  text: { 
    fontSize: 17, 
    paddingBottom:10,
    paddingLeft:10,
    paddingRight:10,
  },
  boxChat:{
     flexDirection:'row-reverse', 
    marginBottom: 5, 
    padding:10, 
    
  },
  boxChat2:{
    flexDirection:'row', 
    marginBottom: 5, 
    padding:10,   
    
  },
  laImage:{
     width:'10%',
     margin: '2.5%',
     height:30,
     minHeight:40,
    //  borderStyle: 'solid',
    //  borderWidth:2,
    //  borderColor: '#fff',
     borderRadius:30,
  },
  textChat:{
    backgroundColor: '#f2fbcce8', 
    width: '85%',
    borderRadius:10,
  },
  
  textChat2:{
    backgroundColor: '#fdf2b2e8', 
    width: '85%',
    borderRadius:10,
  },
  input:{
   backgroundColor: '#fff',
   width:'82%',
   fontSize:20,
   marginLeft:'3%',
   paddingLeft:10,
   height:50,
   marginTop:10,
   borderRadius:25,
   marginBottom:8,
   borderStyle: 'solid',
     borderWidth:1,
     borderColor: '#d5d9de',
     opacity:0.9,
  },
});


export default App;

